
<span style="font-size: 18pt; color: #564D91; font-family: monospace">{{title}}</span>

> [!authors]-
> {{authors}}

> [!article] Journal
> {{publicationTitle}} ({{date | format("YYYY")}})

> [!abstract]-
> {{abstractNote}}

> [!web] Web Content
> URL: [{{url}}]({{url}})
%%> DOI: [{{doi}}]({{doi}})%%
