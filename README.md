# Install Obsidian and open the Repository

This repository is supposed to be opened with Obsidian. If you want to use another Markdown editor, you can so do, but we highly suggest using Obsidian to be able to take full advantage of all its tools.

To download Obsidian, visit:
`https://obsidian.md/download`
You will get a suggestion depending on your operating system.

If you launch Obsidian you will be greeted with a prompt asking you to **Open a Vault**. Click on **Open Vault** and select the directory this file is located in.
If you are asked to trust the authors and load the embeded plugins, click **Yes**.

You now have opened the project in Obsidian, so please open this README file in Obsidian to make sure the format is fine from here onwards.


# Structure of the Repository

> [!repository] 
> > [!folder] Manuscript
> > This is where your manuscript lives. Each file in here represents a part of you manuscript. You can slice and divide your manuscript into as many files as you want.
> 
> > [!folder] Media
> > This folder is the default place where pasted images are stored. You can past images directly from your clipboard or drag it into the folder through the file explore/finder App.
> 
> > [!folder] References
> > This folder is where your References are put into. We highly suggest the Zotero Plugin (which is already installed and configured) to import your References. This is described in more details late in the README.
> 
> > [!folder] Templates
> > This folder holds some sample templates. You can add as many templates as you want. I have already added templates for three authors. To add more templates, take a look at the Callout Section of this README.


# Using Obsidian
## Images
To include images in your manuscript, you can either 
- Paste the directly from your clipboard. This will add the image to the `Media/` folder and create a link in that displays the image in your manuscript.
- Directly store the image in the `Media/` folder through your file management app. To place them into your manuscript, press **Command + L**(Mac) or **Control + L**(Win) and start typing the name of the image file. Afterwards, place a **!** before the link to show the link as image.

If you are not satisfied with the size of the image, you can set its width by adding `|300` after the filename to resize the image to 300 pixels width. Obviously all other widths also work. The link should look like this:
`![[imagefile.jpg|300]`

## Tables
To add a a table, right click on an empty space of the manuscript and select *Insert* → *Table*. You can also click the table icon on the left ribbon bar. Tables will automatically adjust width and height. For further cell formatting besides **bold** *italic*, take a look at the documentation of the *Sheets Extended* [documentation](https://github.com/NicoNekoru/obsidan-advanced-table-xt).

## Callouts
Callouts are small info boxes you can use to highlight certain parts of your manuscript, add a comment, organize your thoughts or create a To-Do task. To do so, you can either 
- click on the *insert callout* icon on the left ribbon bar
- make an empty line in the manuscript, right click on the empty line and select *Insert*→*Callout*.
When the Callout manager window opens, navigate to the callout you want to insert and press the right arrow button.

> [!tip]
> You can create templates for callouts you regularly use. I have already created some templates for To-Dos and author comments. You can then insert callouts easily via *Templater*.

## Importing References
To import references, I highly suggest using Zotero to take full advantage of the installed Zotero Integration plugin. However, you can also import your references manually which probably consumes more time but you don’t have to migrate your full library to Zotero.
### If you already use Zotero:
In Zotero, open the Settings and navigate to the Citing tab. If you don’t have it installed already, click on *additional cite styles* and search for *Springer - Basic (note)*. Add this cite style and you are good to go. 

> [!danger]
> Make sure that PDF Utility is enables and downloaded in the Zotero Integration Plugin settings! In the Obsidian settings scroll down all the way to the bottom of the left sidebar. Click on Zotero Integration and the PDF Utility button is right at the top right.
> 

You can set a hotkey for importing a Zotero entry in the hotkeys section of the Settings. I declared **Option + I** (Mac) or **Alt + I** (Win) as default hotkey. Pressing this opens the Article selection of Zotero. Type in the name of your article or author name. You can also add multiple articles to the importer. Press Enter when you are finished and the plugin will import all listed literature.

The literature will be automatically linked in the manuscript after running the Zotero Intergration. The plugin has created a file for each literature piece named after its citekey. 

## If you are not using Zotero
To add a new literature entry manually, press **Option + Shift + T** (Mac) or **Alt + Shift + T** (Win) and type in the desired cite key. A new window should open to the side of your manuscript where you can fill in Title, Author, Journal, Year and any additional information. If you are done, you can close the split view. This will automatically insert a link to the new entry in your manuscript.

## Adding links to existing entries
To link it at a later point, press **Option + L** (Mac) or **Alt + L** (Win) and start typing the citekey.
